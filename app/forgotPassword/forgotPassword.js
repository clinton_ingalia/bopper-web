/**
 * Created by clintonb on 9/19/2017.
 */

'use strict';

angular.module('bopperLife.forgotPassword', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/forgotPassword', {
            templateUrl: 'forgotPassword/forgotPassword.html',
            controller: 'forgotPasswordCtrl'
        });
    }])

    .controller('forgotPasswordCtrl', [function() {


    }]);