/**
 * Created by clintonb on 9/25/2017.
 */
angular.module('bopperLife.rsvp', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/rsvp', {
            templateUrl: 'events/rsvp.html',
            controller: 'rsvpCtrl'
        });
    }])

    .controller('rsvpCtrl', [function() {


    }]);