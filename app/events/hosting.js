/**
 * Created by clintonb on 9/25/2017.
 */
angular.module('bopperLife.hosting', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/hosting', {
            templateUrl: 'events/hosting.html',
            controller: 'hostingCtrl'
        });
    }])

    .controller('hostingCtrl', [function() {


    }]);