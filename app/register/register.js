/**
 * Created by clintonb on 9/19/2017.
 */
angular.module('bopperLife.register', ['ngRoute', 'toaster'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/register', {
            templateUrl: 'register/register.html',
            controller: 'registerCtrl'
        });
    }])

    .controller('registerCtrl', ['$scope', 'myUrl', 'toaster', '$http', '$location', function($scope, myUrl, toaster, $http, $location) {

        //simple user registration model
        $scope.baseurl = myUrl;
        $scope.user = {
            UserName:"",
            FullNames:"",
            Email:"",
            PhoneNumber:"",
            Password:"",
            ConfirmPassword:""
        };


        //validation rules
        $scope.userNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "User name is required"
            }]
        };

        $scope.fullNamesValidationRules = {
            validationRules: [{
                type: "required",
                message: "Full names is required"
            }]
        };

        $scope.emailValidationRules = {
            validationRules: [{
                type: "required",
                message: "Email is required"
            }]
        };

        $scope.phoneNumberValidationRules = {
            validationRules: [{
                type: "required",
                message: "Phone number is required"
            }]
        };

        $scope.passwordValidationRules = {
            validationRules: [{
                type: "required",
                message: "Password is required"
            }]
        };

        $scope.confirmPasswordValidationRules = {
            validationRules: [{
                type: "compare",
                comparisonTarget: function () {
                    var password = $("#password-validation").dxTextBox("instance");
                    if (password) {
                        return password.option("value");
                    }
                },
                message: "'Password' and 'Confirm Password' do not match."
            },
                {
                    type: "required",
                    message: "Confirm Password is required"
                }]
        };

        $scope.checkValidationRules = {
            validationRules: [{
                type: "compare",
                comparisonTarget: function(){ return true; },
                message: "You must agree to the Terms and Conditions"
            }]
        };


        //input object
        $scope.userRegisterInput = {
            UserName: {
                placeholder: "Enter User Name",
                showClearButton: true
            },
            FullNames: {
                placeholder: "Enter Full Name",
                showClearButton: true
            },
            Email: {
                placeholder: "Enter Email",
                showClearButton: true
            },
            password: {
                mode: "password",
                placeholder: "Enter password",
                showClearButton: true
            },
            phoneNumber: {
                mode: "tel",
                placeholder: "Enter mobile number 0712 345 678",
                showClearButton: true
            },
        };

        //submit button
        $scope.submitButtonOptions = {
            text: 'Register',
            type: 'success',
            useSubmitBehavior: true
        };

        $scope.onFormSubmit = function(e) {
            
            $http.post(myUrl + "api/CreateClient", $scope.user).
            then(function(response) {
                toaster.pop('success', "Registration Successfull", "Welcome to Bopper Life, please proceed to login");
                $location.path('/login');
            }, function(response) {

                if(response.statusCode == 409){
                    toaster.pop('error', data, "User already exists");
                } else {
                    toaster.pop('error', "An error occurred", "Please try again or use different username");
                }
            });

            e.preventDefault();
        };

    }]);
