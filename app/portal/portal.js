/**
 * Created by clintonb on 9/19/2017.
 */
angular.module('bopperLife.portal', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/portal', {
            templateUrl: 'portal/portal.html',
            controller: 'portalCtrl'
        });
    }])

    .controller('portalCtrl', [function() {


    }]);