/**
 * Created by clintonb on 9/25/2017.
 */
angular.module('bopperLife.eventsReport', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/eventsReport', {
            templateUrl: 'reports/eventsReport.html',
            controller: 'eventsReportCtrl'
        });
    }])

    .controller('eventsReportCtrl', [function() {


    }]);