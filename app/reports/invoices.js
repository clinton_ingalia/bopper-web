/**
 * Created by clintonb on 9/25/2017.
 */
angular.module('bopperLife.invoices', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/invoices', {
            templateUrl: 'reports/invoices.html',
            controller: 'invoicesCtrl'
        });
    }])

    .controller('invoicesCtrl', [function() {


    }]);