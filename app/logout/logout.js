/**
 * Created by clintonb on 9/25/2017.
 */
angular.module('bopperLife.logout', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/logout', {
            templateUrl: 'logout/logout.html',
            controller: 'logoutCtrl'
        });
    }])

    .controller('logoutCtrl', [function() {


    }]);