/**
 * Created by clintonb on 9/19/2017.
 */
'use strict';

angular.module('bopperLife.login', ['ngRoute', 'toaster'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'login/login.html',
            controller: 'loginCtrl'
        });
    }])

    .controller('loginCtrl', ['$scope', 'myUrl', 'toaster', '$http', '$location', 'Facebook', function ($scope, myUrl, toaster, $http, $location, Facebook) {
        //simple user login model
        $scope.baseurl = myUrl;
        $scope.user = {
            UserName: "",
            Password: ""
        };

        //validation rules
        $scope.userNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "User name is required"
            }]
        };

        $scope.passwordValidationRules = {
            validationRules: [{
                type: "required",
                message: "Password is required"
            }]
        };

        //input object
        $scope.userRegisterInput = {
            UserName: {
                placeholder: "Enter User Name",
                showClearButton: true
            },
            password: {
                mode: "password",
                placeholder: "Enter password",
                showClearButton: true
            }
        };

        //submit button
        $scope.submitButtonOptions = {
            text: 'Login',
            type: 'success',
            useSubmitBehavior: true
        };

        $scope.onFormSubmit = function (e) {

            $http.post(myUrl + "api/LoginClient", $scope.user).
                then(function (response) {
                    toaster.pop('success', "Login Successfull", "Welcome to Bopper Life");
                    console.log(response.data);
                    $location.path('/portal');
                }, function (response) {

                    if (response.statusCode == 409) {
                        toaster.pop('error', data, "User already exists");
                    } else {
                        toaster.pop('error', "An error occurred", "Please try again or use different username");
                    }
                });

            e.preventDefault();
        };

        //Google login
        $scope.options = {
            'onsuccess': function (response) {
                console.log('Google login successfull'); 
                //get profile info
                var profile = response.getBasicProfile();
                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

                //user
                $scope.userFacebook = {
                    fullNames: profile.getName(),
                    email: profile.getEmail(),
                    profPic: profile.getImageUrl()
                };

                //login or register user
                $http.post(myUrl + "api/LoginRegistration", $scope.userFacebook).
                then(function (response) {
                    toaster.pop('success', "Login Successfull", "Welcome to Bopper Life");
                    console.log(response.data);
                    // $location.path('/portal');
                }, function (response) {

                    if (response.statusCode == 409) {
                        toaster.pop('error', data, "User already exists");
                    } else {
                        toaster.pop('error', "An error occurred", "Please try again or use different username");
                    }
                });
            }
        }


        function onSuccess(googleUser) {
            console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
        }
        function onFailure(error) {
            console.log(error);
        }
        function renderButton() {
            gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': 240,
                'height': 50,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
            });
        }


        //facebook login
        Facebook.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                console.log("Facebook login successful");
                Facebook.api('/me?fields=id,email,name', function (response) {

                    $scope.facebookUser = response;
                    console.log($scope.facebookUser);
                    //incomplete login
                });
            } else {
                console.log("Facebook login unsuccessful");
            }
        });

        // Facebook.getLoginStatus(function (response) {
        //     if (response.status === 'connected') {
        //         console.log(response.authResponse.accessToken);
        //     }
        // });

        $scope.login = function () {
            // From now on you can use the Facebook service just as Facebook api says
            Facebook.login(function (response) {
                // Do something with response.
                console.log(response);
                $scope.userData();
            });
        };

        $scope.getLoginStatus = function () {
            Facebook.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    $scope.loggedIn = true;
                } else {
                    $scope.loggedIn = false;
                }
            });
        };

        $scope.userData = function () {
            Facebook.api('/me?fields=id,email,name', function (response) {

                $scope.facebookUser = response;
                console.log($scope.facebookUser);
            });
        };

    }])
    .directive('googleSignInButton', function () {
        return {
            scope: {
                buttonId: '@',
                options: '&'
            },
            template: '<div></div>',
            link: function (scope, element, attrs) {
                var div = element.find('div')[0];
                div.id = attrs.buttonId;
                gapi.signin2.render(div.id, scope.options()); //render a google button, first argument is an id, second options
            }
        };
    });