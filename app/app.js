'use strict';

// Declare app level module which depends on views, and components
angular.module('bopperLife', [
    'ngRoute',
    'dx',
    'facebook',
    'bopperLife.login',
    'bopperLife.register',
    'bopperLife.forgotPassword',
    'bopperLife.portal',
    'bopperLife.chat',
    'bopperLife.hosting',
    'bopperLife.rsvp',
    'bopperLife.notification',
    'bopperLife.profile',
    'bopperLife.eventsReport',
    'bopperLife.invoices',
    'bopperLife.logout'
])
.constant('myUrl', 'http://localhost:3931/').config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
// .constant('myUrl', 'http://bopperliveapi.azurewebsites.net/').config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {

  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/login'});
}])
.config(function(FacebookProvider) {
  // Set your appId through the setAppId method or
  // use the shortcut in the initialize method directly.
  FacebookProvider.init('1932901927024820');
});
