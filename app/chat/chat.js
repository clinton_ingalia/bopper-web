/**
 * Created by clintonb on 9/25/2017.
 */
angular.module('bopperLife.chat', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/chat', {
            templateUrl: 'chat/chat.html',
            controller: 'chatCtrl'
        });
    }])

    .controller('chatCtrl', [function() {


    }]);