/**
 * Created by clintonb on 9/25/2017.
 */
angular.module('bopperLife.profile', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/profile', {
            templateUrl: 'profile/profile.html',
            controller: 'profileCtrl'
        });
    }])

    .controller('profileCtrl', [function() {


    }]);