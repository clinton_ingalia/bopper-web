/**
 * Created by clintonb on 9/25/2017.
 */
angular.module('bopperLife.notification', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/notification', {
            templateUrl: 'notification/notification.html',
            controller: 'notificationCtrl'
        });
    }])

    .controller('notificationCtrl', [function() {


    }]);